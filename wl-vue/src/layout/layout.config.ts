
export const getMenus = () => {
  return [
    {
      label: "订单管理",
      path: "/order",
      children: [
        {
          label: "订单列表",
          path: "/order/list",
          component: () => import("./../views/order/list.vue"),
        },
        {
          label: "订单日志",
          path: "/order/log",
          component: () => import("./../views/order/log.vue"),
        },
      ],
    },
    {
      label: "物流配送",
      path: "/express",
      children: [
        {
          label: "快递公司",
          path: "/express/company",
          component: () => import("./../views/express/company.vue"),
        },
        {
          label: "快递网点",
          path: "/express/station",
          component: () => import("./../views/express/station.vue"),
        },
        {
          label: "快递员工",
          path: "/express/staff",
          component: () => import("./../views/express/staff.vue"),
        },
        {
          label: "快递包裹",
          path: "/express/package",
          component: () => import("./../views/express/package.vue"),
        },
        {
          label: "快递轨迹",
          path: "/express/track",
          component: () => import("./../views/express/track.vue"),
        },
      ],
    },
    {
      label: "客户服务",
      path: "/customer",
      children: [
        {
          label: "客户列表",
          path: "/customer/list",
          // component: () => import("./../views/customer/list.vue"),
        },
        {
          label: "客服列表",
          path: "/customer/service",
          // component: () => import("./../views/customer/service.vue"),
        },
        {
          label: "服务单列表",
          path: "/service/ticket",
          // component: () => import("./../views/service/ticket.vue"),
        },
        {
          label: "服务消息",
          path: "/service/message",
          // component: () => import("./../views/service/message.vue"),
        },
      ],
    },
    {
      label: "财务结算",
      path: "/finance",
      children: [
        {
          label: "支付列表",
          path: "/payment/list",
          // component: () => import("./../views/payment/list.vue"),
        },
        {
          label: "支出列表",
          path: "/expense/list",
          // component: () => import("./../views/expense/list.vue"),
        },
        {
          label: "结算列表",
          path: "/settlement/list",
          // component: () => import("./../views/settlement/list.vue"),
        },
      ],
    },
    {
      label: "数据分析",
      path: "/analysis",
      children: [
        {
          label: "订单分析",
          path: "/order/analysis",
          // component: () => import("./../views/order/analysis.vue"),
        },
        {
          label: "物流分析",
          path: "/express/analysis",
          // component: () => import("./../views/express/analysis.vue"),
        },
        {
          label: "客户分析",
          path: "/customer/analysis",
          // component: () => import("./../views/customer/analysis.vue"),
        },
        {
          label: "客服分析",
          path: "/service/analysis",
          // component: () => import("./../views/service/analysis.vue"),
        },
      ],
    },
  ]
}
