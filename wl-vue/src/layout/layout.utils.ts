import { Router } from "vue-router";

/**
 *  动态添加路由
 */
export const generateRotue = (menus: any[], router: Router) => {
  menus.forEach((item) => {
    /**
     *  动态添加路由
     */
    if (item.component) {
      /**
       * 添加嵌套路由
       * 第一个参数是父级路由的name
       * 第二个参数 是当前路由信息
       *
       *  */
      router.addRoute("layout", {
        ...item,
        meta: {
          label: item.label,
        },
      });
    }

    if (item.children) {
      generateRotue(item.children, router);
    }
  });
};

