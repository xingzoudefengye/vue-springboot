import { RouteRecordRaw, createRouter, createWebHashHistory } from "vue-router";
import { generateRotue } from "./layout/layout.utils";
import { getMenus } from "./layout/layout.config";

const routes: RouteRecordRaw[] = [
  {
    path: "/",
    name:"layout",
    component: () => import('./layout/layout.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

// 动态生成路由并添加
generateRotue(getMenus(), router);



export default router