package com.wang.kuaidi.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 物流分析表
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ExpressAnalysis对象", description="物流分析表")
public class ExpressAnalysis implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "物流分析ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "分析维度（如时间，地区，网点，员工等）")
    private String dimension;

    @ApiModelProperty(value = "分析指标（如物流量，物流费用，物流时效，物流满意度等）")
    private String indicator;

    @ApiModelProperty(value = "分析结果值")
    private BigDecimal value;


}
