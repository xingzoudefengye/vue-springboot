package com.wang.kuaidi.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 快递轨迹表
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ExpressTrack对象", description="快递轨迹表")
public class ExpressTrack implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "快递轨迹ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "所属快递包裹ID")
    private Long packageId;

    @ApiModelProperty(value = "操作人员姓名")
    private String operatorName;

    @ApiModelProperty(value = "操作时间")
    private Date operatorTime;

    @ApiModelProperty(value = "操作状态（0揽件，1发件，2到达，3派件，4签收）")
    private Integer operatorStatus;


}
