package com.wang.kuaidi.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 客服表
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CustomerService对象", description="客服表")
public class CustomerService implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "客服ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "客服姓名")
    private String serviceName;

    @ApiModelProperty(value = "客服工号")
    private String serviceNo;

    @ApiModelProperty(value = "客服电话")
    private String servicePhone;


}
