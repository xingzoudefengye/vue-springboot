package com.wang.kuaidi.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 服务单表
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ServiceTicket对象", description="服务单表")
public class ServiceTicket implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "服务单ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "服务单号")
    private String ticketNo;

    @ApiModelProperty(value = "服务类型（0咨询，1投诉，2退款，3换货，4其他）")
    private Integer ticketType;

    @ApiModelProperty(value = "服务内容")
    private String ticketContent;

    @ApiModelProperty(value = "服务状态（0待处理，1处理中，2已完成，3已取消）")
    private Integer ticketStatus;

    @ApiModelProperty(value = "所属客户ID")
    private Long customerId;

    @ApiModelProperty(value = "所属客服ID")
    private Long serviceId;


}
