package com.wang.kuaidi.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 快递员工表
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ExpressStaff对象", description="快递员工表")
public class ExpressStaff implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "快递员工ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "快递员工姓名")
    private String staffName;

    @ApiModelProperty(value = "快递员工工号")
    private String staffNo;

    @ApiModelProperty(value = "快递员工电话")
    private String staffPhone;

    @ApiModelProperty(value = "所属快递网点ID")
    private Long stationId;


}
