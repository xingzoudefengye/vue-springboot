package com.wang.kuaidi.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 支出表
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Expense对象", description="支出表")
public class Expense implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "支出ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "支出单号")
    private String expenseNo;

    @ApiModelProperty(value = "支出类型（0工资，1物料，2水电费，3其他）")
    private Integer expenseType;

    @ApiModelProperty(value = "支出金额")
    private BigDecimal expenseAmount;


}
