package com.wang.kuaidi.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 客户分析表
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CustomerAnalysis对象", description="客户分析表")
public class CustomerAnalysis implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "客户分析ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "分析维度（如时间，地区，性别，年龄等）")
    private String dimension;

    @ApiModelProperty(value = "分析指标（如客户量，客户消费，客户忠诚度，客户满意度等）")
    private String indicator;

    @ApiModelProperty(value = "分析结果值")
    private BigDecimal value;


}
