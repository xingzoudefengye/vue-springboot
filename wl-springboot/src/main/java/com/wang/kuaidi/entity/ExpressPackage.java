package com.wang.kuaidi.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 快递包裹表
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ExpressPackage对象", description="快递包裹表")
public class ExpressPackage implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "快递包裹ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "运单号")
    private String packageNo;

    @ApiModelProperty(value = "寄件人姓名")
    private String senderName;

    @ApiModelProperty(value = "寄件人电话")
    private String senderPhone;

    @ApiModelProperty(value = "寄件人地址")
    private String senderAddress;

    @ApiModelProperty(value = "收件人姓名")
    private String receiverName;

    @ApiModelProperty(value = "收件人电话")
    private String receiverPhone;

    @ApiModelProperty(value = "收件人地址")
    private String receiverAddress;

    @ApiModelProperty(value = "包裹重量")
    private BigDecimal packageWeight;

    @ApiModelProperty(value = "包裹费用")
    private BigDecimal packageFee;

    @ApiModelProperty(value = "所属快递网点ID")
    private Long stationId;

    @ApiModelProperty(value = "所属快递员工ID")
    private Long staffId;


}
