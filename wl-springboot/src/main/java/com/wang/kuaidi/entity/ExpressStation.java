package com.wang.kuaidi.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 快递网点表
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ExpressStation对象", description="快递网点表")
public class ExpressStation implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "快递网点ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "快递网点名称")
    private String stationName;

    @ApiModelProperty(value = "快递网点地址")
    private String stationAddress;

    @ApiModelProperty(value = "快递网点电话")
    private String stationPhone;


}
