package com.wang.kuaidi.controller;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wang.kuaidi.entity.OrderInfo;
import com.wang.kuaidi.service.impl.OrderInfoServiceImpl;
import com.wang.utils.ResultEntity;
import com.wang.utils.UUIDUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@RestController
@Slf4j
@RequestMapping("/kuaidi/order-info")
public class OrderInfoController {
        @Autowired
        OrderInfoServiceImpl service;

        @PostMapping("/getList")
        @ApiOperation(value = "查询列表", notes = "查询列表")
        public Object getList(@RequestBody Map<String, Object> mapParam) {
            OrderInfo obj = JSON.parseObject(JSON.toJSONString(mapParam), OrderInfo.class);
            log.info("obj:" + JSON.toJSONString(obj));
            // 从params中获取分页参数
            int pageNum = mapParam.get("pageNum") != null ? (int) mapParam.get("pageNum") : 1;
            int pageSize = mapParam.get("pageSize") != null ? (int) mapParam.get("pageSize") : 5;

            log.info(JSON.toJSONString("pageNum:" + pageNum));
            QueryWrapper<OrderInfo> queryWrapper = new QueryWrapper<>();
            if (obj.getOrderNo() != null) {
                queryWrapper.like("order_no", obj.getOrderNo());
            }

            Page<OrderInfo> page = new Page<>(pageNum, pageSize);
            IPage<OrderInfo> SysUserList = service.page(page, queryWrapper);
            return ResultEntity.ok(SysUserList);
        }


        @PostMapping("/add")
        @ApiOperation(value = "添加", notes = "添加")
        public Object add(@RequestBody  OrderInfo obj) {
            if (Objects.isNull(obj)) {
                return ResultEntity.fail("入参不全");
            }
            log.info("接口add");
            if (obj.getId() == null) {
                obj.setOrderNo(UUIDUtils.getStr());
            }
            service.saveOrUpdate(obj);
            return ResultEntity.ok();
        }

        @PostMapping("/getByKey")
        @ApiOperation(value = "查询详情", notes = "查询详情")
        @ApiResponses({@ApiResponse(code = 2000, message = "成功")})
        public Object getByKey(@RequestBody  OrderInfo obj) throws Exception {
            obj = service.getById(obj.getId());
            return ResultEntity.ok(obj);
        }

        @PostMapping("/delByKeys")
        @ApiOperation(value = "根据主键删除", notes = "根据主键删除")
        @ApiResponses({@ApiResponse(code = 2000, message = "成功")})
        public Object delByKeys(@RequestBody List<Integer> ids) throws Exception {
            boolean b = service.removeByIds(ids);
            return ResultEntity.ok(b);
        }
}
