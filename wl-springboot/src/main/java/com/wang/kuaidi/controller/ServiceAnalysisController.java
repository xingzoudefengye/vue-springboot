package com.wang.kuaidi.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 客服分析表 前端控制器
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@RestController
@RequestMapping("/kuaidi/service-analysis")
public class ServiceAnalysisController {

}
