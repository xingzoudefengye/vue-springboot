package com.wang.kuaidi.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 客户分析表 前端控制器
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@RestController
@RequestMapping("/kuaidi/customer-analysis")
public class CustomerAnalysisController {

}
