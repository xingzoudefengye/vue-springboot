package com.wang.kuaidi.mapper;

import com.wang.kuaidi.entity.OrderLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单日志表 Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface OrderLogMapper extends BaseMapper<OrderLog> {

}
