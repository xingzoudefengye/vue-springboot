package com.wang.kuaidi.mapper;

import com.wang.kuaidi.entity.Customer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 客户表 Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface CustomerMapper extends BaseMapper<Customer> {

}
