package com.wang.kuaidi.mapper;

import com.wang.kuaidi.entity.ExpressTrack;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 快递轨迹表 Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface ExpressTrackMapper extends BaseMapper<ExpressTrack> {

}
