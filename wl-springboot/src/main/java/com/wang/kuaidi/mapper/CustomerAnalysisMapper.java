package com.wang.kuaidi.mapper;

import com.wang.kuaidi.entity.CustomerAnalysis;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 客户分析表 Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface CustomerAnalysisMapper extends BaseMapper<CustomerAnalysis> {

}
