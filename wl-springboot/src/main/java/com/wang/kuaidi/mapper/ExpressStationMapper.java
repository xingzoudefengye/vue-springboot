package com.wang.kuaidi.mapper;

import com.wang.kuaidi.entity.ExpressStation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 快递网点表 Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface ExpressStationMapper extends BaseMapper<ExpressStation> {

}
