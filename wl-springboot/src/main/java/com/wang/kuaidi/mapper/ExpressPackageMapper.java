package com.wang.kuaidi.mapper;

import com.wang.kuaidi.entity.ExpressPackage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 快递包裹表 Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface ExpressPackageMapper extends BaseMapper<ExpressPackage> {

}
