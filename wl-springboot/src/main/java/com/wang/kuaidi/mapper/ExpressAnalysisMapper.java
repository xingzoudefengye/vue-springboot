package com.wang.kuaidi.mapper;

import com.wang.kuaidi.entity.ExpressAnalysis;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 物流分析表 Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface ExpressAnalysisMapper extends BaseMapper<ExpressAnalysis> {

}
