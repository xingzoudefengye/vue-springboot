package com.wang.kuaidi.mapper;

import com.wang.kuaidi.entity.Payment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付表 Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface PaymentMapper extends BaseMapper<Payment> {

}
