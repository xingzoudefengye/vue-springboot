package com.wang.kuaidi.mapper;

import com.wang.kuaidi.entity.OrderInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {

}
