package com.wang.kuaidi.mapper;

import com.wang.kuaidi.entity.CustomerService;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 客服表 Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface CustomerServiceMapper extends BaseMapper<CustomerService> {

}
