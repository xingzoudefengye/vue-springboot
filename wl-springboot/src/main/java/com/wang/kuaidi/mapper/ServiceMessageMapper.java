package com.wang.kuaidi.mapper;

import com.wang.kuaidi.entity.ServiceMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 服务消息表 Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface ServiceMessageMapper extends BaseMapper<ServiceMessage> {

}
