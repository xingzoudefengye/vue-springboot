package com.wang.kuaidi.mapper;

import com.wang.kuaidi.entity.Settlement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 结算表 Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface SettlementMapper extends BaseMapper<Settlement> {

}
