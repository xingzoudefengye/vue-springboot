package com.wang.kuaidi.mapper;

import com.wang.kuaidi.entity.OrderItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单明细表 Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface OrderItemMapper extends BaseMapper<OrderItem> {

}
