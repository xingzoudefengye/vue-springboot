package com.wang.kuaidi.mapper;

import com.wang.kuaidi.entity.ExpressStaff;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 快递员工表 Mapper 接口
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface ExpressStaffMapper extends BaseMapper<ExpressStaff> {

}
