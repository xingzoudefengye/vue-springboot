package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.Payment;
import com.wang.kuaidi.mapper.PaymentMapper;
import com.wang.kuaidi.service.IPaymentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class PaymentServiceImpl extends ServiceImpl<PaymentMapper, Payment> implements IPaymentService {

}
