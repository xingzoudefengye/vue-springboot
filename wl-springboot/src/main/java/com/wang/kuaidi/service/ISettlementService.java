package com.wang.kuaidi.service;

import com.wang.kuaidi.entity.Settlement;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 结算表 服务类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface ISettlementService extends IService<Settlement> {

}
