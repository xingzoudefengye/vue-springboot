package com.wang.kuaidi.service;

import com.wang.kuaidi.entity.ExpressStation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 快递网点表 服务类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface IExpressStationService extends IService<ExpressStation> {

}
