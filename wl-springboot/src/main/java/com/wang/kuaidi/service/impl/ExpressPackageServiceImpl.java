package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.ExpressPackage;
import com.wang.kuaidi.mapper.ExpressPackageMapper;
import com.wang.kuaidi.service.IExpressPackageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 快递包裹表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class ExpressPackageServiceImpl extends ServiceImpl<ExpressPackageMapper, ExpressPackage> implements IExpressPackageService {

}
