package com.wang.kuaidi.service;

import com.wang.kuaidi.entity.ExpressPackage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 快递包裹表 服务类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface IExpressPackageService extends IService<ExpressPackage> {

}
