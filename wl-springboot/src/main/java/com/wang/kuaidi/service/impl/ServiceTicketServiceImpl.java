package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.ServiceTicket;
import com.wang.kuaidi.mapper.ServiceTicketMapper;
import com.wang.kuaidi.service.IServiceTicketService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务单表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class ServiceTicketServiceImpl extends ServiceImpl<ServiceTicketMapper, ServiceTicket> implements IServiceTicketService {

}
