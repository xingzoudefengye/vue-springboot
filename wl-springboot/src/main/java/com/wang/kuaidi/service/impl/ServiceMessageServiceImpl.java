package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.ServiceMessage;
import com.wang.kuaidi.mapper.ServiceMessageMapper;
import com.wang.kuaidi.service.IServiceMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务消息表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class ServiceMessageServiceImpl extends ServiceImpl<ServiceMessageMapper, ServiceMessage> implements IServiceMessageService {

}
