package com.wang.kuaidi.service;

import com.wang.kuaidi.entity.ServiceAnalysis;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 客服分析表 服务类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface IServiceAnalysisService extends IService<ServiceAnalysis> {

}
