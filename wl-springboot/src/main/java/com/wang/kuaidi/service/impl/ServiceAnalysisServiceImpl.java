package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.ServiceAnalysis;
import com.wang.kuaidi.mapper.ServiceAnalysisMapper;
import com.wang.kuaidi.service.IServiceAnalysisService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客服分析表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class ServiceAnalysisServiceImpl extends ServiceImpl<ServiceAnalysisMapper, ServiceAnalysis> implements IServiceAnalysisService {

}
