package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.ExpressStaff;
import com.wang.kuaidi.mapper.ExpressStaffMapper;
import com.wang.kuaidi.service.IExpressStaffService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 快递员工表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class ExpressStaffServiceImpl extends ServiceImpl<ExpressStaffMapper, ExpressStaff> implements IExpressStaffService {

}
