package com.wang.kuaidi.service;

import com.wang.kuaidi.entity.CustomerService;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 客服表 服务类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface ICustomerServiceService extends IService<CustomerService> {

}
