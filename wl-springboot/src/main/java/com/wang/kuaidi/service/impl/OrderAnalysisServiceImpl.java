package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.OrderAnalysis;
import com.wang.kuaidi.mapper.OrderAnalysisMapper;
import com.wang.kuaidi.service.IOrderAnalysisService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单分析表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class OrderAnalysisServiceImpl extends ServiceImpl<OrderAnalysisMapper, OrderAnalysis> implements IOrderAnalysisService {

}
