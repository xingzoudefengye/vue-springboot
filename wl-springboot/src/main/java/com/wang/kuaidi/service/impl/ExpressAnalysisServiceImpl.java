package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.ExpressAnalysis;
import com.wang.kuaidi.mapper.ExpressAnalysisMapper;
import com.wang.kuaidi.service.IExpressAnalysisService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 物流分析表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class ExpressAnalysisServiceImpl extends ServiceImpl<ExpressAnalysisMapper, ExpressAnalysis> implements IExpressAnalysisService {

}
