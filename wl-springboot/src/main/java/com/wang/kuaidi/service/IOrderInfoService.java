package com.wang.kuaidi.service;

import com.wang.kuaidi.entity.OrderInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface IOrderInfoService extends IService<OrderInfo> {

}
