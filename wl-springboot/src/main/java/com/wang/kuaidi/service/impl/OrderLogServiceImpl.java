package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.OrderLog;
import com.wang.kuaidi.mapper.OrderLogMapper;
import com.wang.kuaidi.service.IOrderLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单日志表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class OrderLogServiceImpl extends ServiceImpl<OrderLogMapper, OrderLog> implements IOrderLogService {

}
