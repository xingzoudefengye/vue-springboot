package com.wang.kuaidi.service;

import com.wang.kuaidi.entity.ExpressStaff;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 快递员工表 服务类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface IExpressStaffService extends IService<ExpressStaff> {

}
