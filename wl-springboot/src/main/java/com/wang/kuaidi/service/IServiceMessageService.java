package com.wang.kuaidi.service;

import com.wang.kuaidi.entity.ServiceMessage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务消息表 服务类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface IServiceMessageService extends IService<ServiceMessage> {

}
