package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.Settlement;
import com.wang.kuaidi.mapper.SettlementMapper;
import com.wang.kuaidi.service.ISettlementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 结算表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class SettlementServiceImpl extends ServiceImpl<SettlementMapper, Settlement> implements ISettlementService {

}
