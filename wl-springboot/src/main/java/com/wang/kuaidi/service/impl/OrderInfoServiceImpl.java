package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.OrderInfo;
import com.wang.kuaidi.mapper.OrderInfoMapper;
import com.wang.kuaidi.service.IOrderInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements IOrderInfoService {

}
