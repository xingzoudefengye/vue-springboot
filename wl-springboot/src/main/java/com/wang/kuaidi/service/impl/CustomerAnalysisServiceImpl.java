package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.CustomerAnalysis;
import com.wang.kuaidi.mapper.CustomerAnalysisMapper;
import com.wang.kuaidi.service.ICustomerAnalysisService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客户分析表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class CustomerAnalysisServiceImpl extends ServiceImpl<CustomerAnalysisMapper, CustomerAnalysis> implements ICustomerAnalysisService {

}
