package com.wang.kuaidi.service;

import com.wang.kuaidi.entity.ExpressAnalysis;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 物流分析表 服务类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface IExpressAnalysisService extends IService<ExpressAnalysis> {

}
