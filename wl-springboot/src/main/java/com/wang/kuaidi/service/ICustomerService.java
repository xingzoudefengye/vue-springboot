package com.wang.kuaidi.service;

import com.wang.kuaidi.entity.Customer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 客户表 服务类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface ICustomerService extends IService<Customer> {

}
