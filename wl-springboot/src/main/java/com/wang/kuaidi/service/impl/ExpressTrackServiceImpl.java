package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.ExpressTrack;
import com.wang.kuaidi.mapper.ExpressTrackMapper;
import com.wang.kuaidi.service.IExpressTrackService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 快递轨迹表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class ExpressTrackServiceImpl extends ServiceImpl<ExpressTrackMapper, ExpressTrack> implements IExpressTrackService {

}
