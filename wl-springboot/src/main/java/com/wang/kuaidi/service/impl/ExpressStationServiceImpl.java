package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.ExpressStation;
import com.wang.kuaidi.mapper.ExpressStationMapper;
import com.wang.kuaidi.service.IExpressStationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 快递网点表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class ExpressStationServiceImpl extends ServiceImpl<ExpressStationMapper, ExpressStation> implements IExpressStationService {

}
