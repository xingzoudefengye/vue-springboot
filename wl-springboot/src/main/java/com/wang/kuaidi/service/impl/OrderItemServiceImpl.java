package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.OrderItem;
import com.wang.kuaidi.mapper.OrderItemMapper;
import com.wang.kuaidi.service.IOrderItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单明细表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class OrderItemServiceImpl extends ServiceImpl<OrderItemMapper, OrderItem> implements IOrderItemService {

}
