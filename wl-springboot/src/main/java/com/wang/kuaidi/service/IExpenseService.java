package com.wang.kuaidi.service;

import com.wang.kuaidi.entity.Expense;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 支出表 服务类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface IExpenseService extends IService<Expense> {

}
