package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.Expense;
import com.wang.kuaidi.mapper.ExpenseMapper;
import com.wang.kuaidi.service.IExpenseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支出表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class ExpenseServiceImpl extends ServiceImpl<ExpenseMapper, Expense> implements IExpenseService {

}
