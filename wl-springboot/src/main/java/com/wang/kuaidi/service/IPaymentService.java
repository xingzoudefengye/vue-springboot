package com.wang.kuaidi.service;

import com.wang.kuaidi.entity.Payment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 支付表 服务类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface IPaymentService extends IService<Payment> {

}
