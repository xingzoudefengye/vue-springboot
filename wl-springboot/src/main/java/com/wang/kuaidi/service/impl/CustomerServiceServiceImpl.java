package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.CustomerService;
import com.wang.kuaidi.mapper.CustomerServiceMapper;
import com.wang.kuaidi.service.ICustomerServiceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客服表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class CustomerServiceServiceImpl extends ServiceImpl<CustomerServiceMapper, CustomerService> implements ICustomerServiceService {

}
