package com.wang.kuaidi.service;

import com.wang.kuaidi.entity.ExpressTrack;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 快递轨迹表 服务类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
public interface IExpressTrackService extends IService<ExpressTrack> {

}
