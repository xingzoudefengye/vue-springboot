package com.wang.kuaidi.service.impl;

import com.wang.kuaidi.entity.Customer;
import com.wang.kuaidi.mapper.CustomerMapper;
import com.wang.kuaidi.service.ICustomerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客户表 服务实现类
 * </p>
 *
 * @author wang
 * @since 2023-06-25
 */
@Service
public class CustomerServiceImpl extends ServiceImpl<CustomerMapper, Customer> implements ICustomerService {

}
