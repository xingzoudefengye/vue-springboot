package com.wang.utils;

import java.util.UUID;

public class UUIDUtils {

    public static int getNum() {
        // Generate a random UUID
        UUID uuid = UUID.randomUUID();
        // Convert the UUID to a string
        String uuidString = uuid.toString();
        // Remove the hyphens from the string
        uuidString = uuidString.replace("-", "");
        // Take the first 10 characters of the string
        uuidString = uuidString.substring(0, 10);
        // Convert the string to a long
        int uuidNumber = Integer.parseInt(uuidString, 16);
        // Print the number
        System.out.println(uuidNumber);
        return  uuidNumber;
    }

    public static String getStr() {
        // Generate a random UUID
        UUID uuid = UUID.randomUUID();
        // Convert the UUID to a string
        String uuidString = uuid.toString();
        // Remove the hyphens from the string
        uuidString = uuidString.replace("-", "");
        // Take the first 10 characters of the string
        uuidString = uuidString.substring(0, 10);
        // Print the number
        System.out.println(uuidString);
        return  uuidString;
    }
}
