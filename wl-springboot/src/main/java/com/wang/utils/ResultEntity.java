package com.wang.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultEntity<T> {

    // 定义一个枚举类型来表示响应类型
    public enum ResultType {
        SUCCESS(2000, "SUCCESS"),
        FAIL(5000, "FAIL");

        private Integer code;
        private String msg;

        ResultType(Integer code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public Integer getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }

    private Integer code;//返回码
    private String msg;//返回消息
    private T content;//返回数据内容

    // 使用枚举类型作为参数
    public ResultEntity(ResultType resultType) {
        this.code = resultType.getCode();
        this.msg = resultType.getMsg();
    }

    // 使用枚举类型和数据作为参数
    public ResultEntity(ResultType resultType, T content) {
        this.code = resultType.getCode();
        this.msg = resultType.getMsg();
        this.content = content;
    }

    // 使用泛型通配符来表示任意类型的数据
    public static ResultEntity<?> ok() {
        return new ResultEntity<>(ResultType.SUCCESS);
    }

    public static ResultEntity<?> ok(Object data) {
        return new ResultEntity<>(ResultType.SUCCESS, data);
    }

    public static ResultEntity<?> fail() {
        return new ResultEntity<>(ResultType.FAIL);
    }

    public static ResultEntity<?> fail(Object data) {
        return new ResultEntity<>(ResultType.FAIL, data);
    }
}
