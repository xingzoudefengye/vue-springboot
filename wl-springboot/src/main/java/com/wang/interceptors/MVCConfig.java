//package com.wang.interceptors;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//@Configuration
//public class MVCConfig implements WebMvcConfigurer {
//    /**
//     * 静态资源映射
//     *
//     * @param registry
//     */
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("swagger-ui.html")
//                .addResourceLocations("classpath:/META-INF/resources/");
//        registry.addResourceHandler("/webjars/**")
//                .addResourceLocations("classpath:/META-INF/resources/webjars/");
//        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
//    }
//
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        String[] paths = {"/decagency/user/login", "/swagger-ui.html",
//                "/swagger-ui/index.html","/webjars/**","/static/**"};
//        registry.addInterceptor(authenticationInterceptor())
//                //表示拦截所有请求
//                .addPathPatterns("/**")
//                //表示取消对特定路径的拦截
//                .excludePathPatterns("/swagger-resources/**","/swagger-ui/**", "/v3/**", "/error","/common/**")
//                .excludePathPatterns(paths);
//
//    }
//
//    @Bean
//    public AuthenticationInterceptor authenticationInterceptor() {
//
//        return new AuthenticationInterceptor();
//    }
//}
