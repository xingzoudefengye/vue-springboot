package com.wang;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@MapperScan("com.wang")
@EnableSwagger2
@SpringBootApplication
public class WangtengApplication {

    public static void main(String[] args) {
        SpringApplication.run(WangtengApplication.class, args);
    }

}
